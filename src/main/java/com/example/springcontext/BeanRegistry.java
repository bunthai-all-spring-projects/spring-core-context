package com.example.springcontext;

import com.example.springcontext.annotations.CommandType;
import com.example.springcontext.annotations.MyLength;
import com.example.springcontext.component.MyTest;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BeanRegistry implements ApplicationContextAware {

    private ApplicationContext applicationContext;
    private static HashMap<String, String> registeredHandlers;
    private static Map<String, List<String>> CORE_EVENTS;

    static {
        registeredHandlers = new HashMap<>();
        CORE_EVENTS = new HashMap<>();
    }




    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//        applicationContext = this.applicationContext;
        this.applicationContext = applicationContext;

        String[] beans = this.applicationContext.getBeanNamesForAnnotation(CommandType.class);

        for (String bean : beans) {
            CommandType commandType = applicationContext.findAnnotationOnBean(bean, CommandType.class);
            this.registeredHandlers.put(commandType.entity() + "|" + commandType.action(), bean);
            CORE_EVENTS.put(commandType.entity(), Arrays.asList(commandType.action()));
       }

//        String[] beans2 = this.applicationContext.getBeanDefinitionNames();

//        String[] beans2 = this.applicationContext.getBeanNamesForAnnotation(MyLength.class);
        String[] beans2 = this.applicationContext.getBeanDefinitionNames();
        for (String bean : beans2) {
            if (applicationContext.findAnnotationOnBean(bean, MyLength.class) instanceof MyLength) {
                MyLength myLength = applicationContext.findAnnotationOnBean(bean, MyLength.class);
                MyTest myTest = applicationContext.getBean(bean, MyTest.class);
                if (myTest.getVal() < myLength.min() && myTest.getVal() > myLength.max())
                    throw new IllegalArgumentException(String.format("val:%s must be between %s and %s", myTest.getVal(), myLength.min(), myLength.max()));
            }
        };
        System.out.println("hello world");

    }
}
