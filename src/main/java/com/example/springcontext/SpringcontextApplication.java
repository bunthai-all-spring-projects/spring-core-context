package com.example.springcontext;

import com.example.springcontext.component.MyTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcontextApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringcontextApplication.class, args);
	}

	@Autowired
	MyTest t;

	@Override
	public void run(String... args) throws Exception {
		t.setVal(8);

		System.out.println(t);
	}
}
