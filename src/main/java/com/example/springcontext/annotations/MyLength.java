package com.example.springcontext.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface MyLength {

    /**
     * Min validate {@link MyLength}.
     */
    int min();

    /**
     * Max validate {@link MyLength}.
     */
    int max();
}
