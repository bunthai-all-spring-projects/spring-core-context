package com.example.springcontext.component;

import com.example.springcontext.annotations.MyLength;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Data
@Service
public class MyTest {
    @MyLength(min = 1, max = 2)
    int val;
}
