package com.example.springcontext.handlers;

import com.example.springcontext.annotations.CommandType;
import org.springframework.stereotype.Service;

@Service
@CommandType(entity = "Employee", action = "CREATE")
public class CreateEmployeeHandler {
}
